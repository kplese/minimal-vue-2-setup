#  Minimal Vue 2 #

In order to get this up and running, please follow the steps described in [How do I get set up?](#markdown-header-how-do-i-get-set-up)

### What is this repository for? ###

This repository shows the basic setup of the Vue 2.0 project with using Vueify, Browserify and Gulp (Laravel Elixir). 
This is a standalone Vue 2.0 project which doesn't require Laravel framework to be installed.
It is based on a tutorial written by [Matt Stauffer](https://blog.tighten.co/setting-up-your-first-vuejs-site-using-laravel-elixir-and-vueify?utm_source=mattstauffer.co).

### How do I get set up? ###

To get started, clone the repository:

```
#!bash

git clone git@bitbucket.org:kplese/minimal-vue-2-setup.git my-vue-project
```

Change directory by executing:
```
#!bash

cd my-vue-project

```


Run the npm install:
```
#!bash

npm install

```

Build with gulp:
```
#!bash

gulp

```

When you're done, open `index.html` in your browser and you should see the basic project working.