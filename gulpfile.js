var elixir = require('laravel-elixir');
require('laravel-elixir-browserify-official');
require('laravel-elixir-vueify-2.0');

elixir.config.assetsPath = 'assets';
elixir.config.publicPath = '';



elixir(function(mix) {
    mix.sass('app.scss')
    .browserify('app.js');
});
