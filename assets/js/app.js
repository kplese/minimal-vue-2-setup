import Vue from 'vue';
import Profile from "./components/Profile.vue";

new Vue({
  el: '#app',
  components: { Profile }
});
